package net.unit8.testharness;

import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Optional;

public class ApiService {
    private final RetryPolicy<String> retryPolicy;
    private final CloseableHttpClient client;

    public ApiService(Logger logger) {
        client = HttpClients.createDefault();
        retryPolicy = new RetryPolicy<String>()
            .handle(IOException.class)
            // If the exception is http client error, don't retry.
            .abortIf((body, exception) -> Optional.of(exception)
                .filter(HttpResponseException.class::isInstance)
                .map(HttpResponseException.class::cast)
                .filter(e -> 400 <= e.getStatusCode() && e.getStatusCode() < 500)
                .isPresent())
            .onAbort(e -> logger.error("Http request abort for a client error", e.getFailure()))
            .onRetry(e -> logger.debug("Retry for {}", e.getLastFailure().getLocalizedMessage()))
            .onRetriesExceeded(e -> logger.warn("Http request abort for a server error, 3 times tries are all failure", e.getFailure()))
            .withMaxRetries(3);
    }

    public String exec() {
        HttpGet httpGet = new HttpGet("http://localhost:8089/my/resource");
        httpGet.addHeader("accept", "application/json");
        RequestConfig config = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(3000).build();
        httpGet.setConfig(config);
        ResponseHandler<String> responseHandler = new BasicResponseHandler();
        return Failsafe
            .with(retryPolicy)
            .get(() -> client.execute(httpGet, responseHandler));
    }
}