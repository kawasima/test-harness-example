package net.unit8.testharness;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.http.client.HttpResponseException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketTimeoutException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class WireMockTest {
    private static final Logger LOG = LoggerFactory.getLogger(WireMockTest.class);
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    /** sut */
    private ApiService service;

    @Before
    public void setup() {
        service = new ApiService(LOG);
    }

    @Test
    public void status200() {
        stubFor(get(urlEqualTo("/my/resource"))
            .withHeader("Accept", equalTo("application/json"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{}"))
        );
        Assertions.assertThat(service.exec()).isEqualTo("{}");
        verify(exactly(1), getRequestedFor(urlEqualTo("/my/resource")));
    }


    
    @Test
    public void status400_noRetry() {
        stubFor(get(urlEqualTo("/my/resource"))
            .withHeader("Accept", equalTo("application/json"))
            .willReturn(aResponse()
                .withStatus(400)
                .withHeader("Content-Type", "application/json")
                .withBody("{}"))
        );
        assertThatThrownBy(() -> service.exec()).hasCauseInstanceOf(HttpResponseException.class);
        verify(exactly(1), getRequestedFor(urlEqualTo("/my/resource")));
    }


    @Test
    public void status500_retry3times() {
        stubFor(get(urlEqualTo("/my/resource"))
            .withHeader("Accept", equalTo("application/json"))
            .willReturn(aResponse()
                .withStatus(500)
                .withHeader("Content-Type", "application/json")
                .withBody("{}"))
        );
        assertThatThrownBy(() -> service.exec()).hasCauseInstanceOf(HttpResponseException.class);
        verify(exactly(4), getRequestedFor(urlEqualTo("/my/resource")));
    }

    @Test
    public void delayResponse() {
        stubFor(get(urlEqualTo("/my/resource"))
            .withHeader("Accept", equalTo("application/json"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("{}")
                .withFixedDelay(10000)));

        assertThatThrownBy(() -> service.exec()).hasCauseInstanceOf(SocketTimeoutException.class);
        verify(exactly(4), getRequestedFor(urlEqualTo("/my/resource")));
    }
}